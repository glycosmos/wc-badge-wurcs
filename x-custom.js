import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';

class XCustom extends PolymerElement {
  static get properties() {
    return {
      user: {
        type: Object,
        value: function(){
          return { id: 'xxx', isAdmin: false };
          // return { id: 'xxx', isAdmin: false };
        }
      }
    };
  }
  ready(){
    super.ready();
    console.log("x-custom: ", this.user.id, this.user.isAdmin);
  }
  static get template() {
    return html`
    <h1>DEMO</h1>
      <p>{{user.isAdmin}}</p>
      <template is="dom-if" if="{{user.isAdmin}}">
        <!-- Only admins will see this. -->
        <p>TRUE CONDITION</p>
      </template>
    `;
  }
}
customElements.define('x-custom', XCustom);
