import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class RepoMyList extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

  <iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/repo_registered_text_list_param_email?email={{email}}&graph=http%3A%2F%2Fsparqlite.com%2Frepo%2Ftest&limit=10&offset=0" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <table border="1">
      <thead>
        <tr>
          <th scope="cols">My List</th>
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <th scope="row"><a href="RepoEntry.html?id={{item.hashed_text}}" target="_blank">[[item.text]]</a>[[item.date]] </th>
          </tr>
        </template>
      </tbody>
    </table>
  </div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      email: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('repo-mylist', RepoMyList);
