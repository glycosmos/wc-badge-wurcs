import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../repo-mylist.js';

class DemoElement extends PolymerElement {
  static get template() {
    return html`
      <style>
  :host {
font-family: sans-serif;
--icon-toggle-color: lightgrey;
--icon-toggle-outline-color: black;
--icon-toggle-pressed-color: red;
}
      </style>
      
      <h3>Repo List Demo</h3>
      <repo-mylist email="sparqlite@email.com"></repo-mylist>
        
    `;
  }
}
customElements.define('demo-element', DemoElement);
